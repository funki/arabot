package arabot;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Vit on 1. 5. 2016.
 */
public class Main {

    public static void runCode() {
        StringBuilder sb = new StringBuilder();
        sb.append("Čest, tady AraBot.\nNadcházející události:\n");
        try {
            sb.append(CalendarPuller.pull("vus6f31796dit7uelsuovh51rk@group.calendar.google.com"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        sb.append("Suplování:\n");
        sb.append(SuplParser.getSubstitutes("2.E"));
        //System.out.println(sb.toString());
        FBPoster.post(sb.toString());
    }

    private static class MyTimeTask extends TimerTask
    {
        @Override
        public void run()
        {
            runCode();
        }
    }

    public static void main(String[] args) throws IOException {
        runCode();
        /*
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH, 1);
        c.set(Calendar.HOUR_OF_DAY, 15);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        Date date = c.getTime();
        Timer t = new Timer();
        t.schedule(new MyTimeTask(), date, 86400000);
        */
    }
}
