package arabot;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Vitek on 2. 5. 2016.
 */
public class SuplParser {

    public static final String URL_STRING = "http://www.gyarab.cz/suplovani/suplobec.htm";

    public static String getSubstitutes(String className){
        StringBuilder sb = new StringBuilder();

        try{
            Document doc = Jsoup.parse(new URL(URL_STRING), 5000);
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DATE, 1);
            SimpleDateFormat format1 = new SimpleDateFormat("d.M.yyyy");

            Element date = doc.select("p:contains("+format1.format(c.getTime())+")").first();
            Element nextSibling = date.nextElementSibling();
            Elements afterDate = new Elements();

            while(nextSibling != null) {
                afterDate.add(nextSibling);
                nextSibling = nextSibling.nextElementSibling();
            }
            boolean flag = false;
            for (Element sibling :afterDate) {
                for (Element table: sibling.getElementsByTag("table")) {
                    if(table.text().contains("Změny v rozvrzích tříd:"))
                    for (Element row: table.getElementsByTag("tr")) {
                        if(row.getElementsByTag("td").get(0).text().contains(className)){
                            sb.append(row.text().substring(3));
                            sb.append("\n");
                            flag = true;
                        } else if(row.getElementsByTag("td").get(0).text().contains("\u00a0")&&flag) {
                            sb.append(row.text());
                            sb.append("\n");
                        } else if(flag){
                            flag = false;
                        }
                    }
                }
            }


        }catch(IOException ioe){
            System.err.println("Connection error!");
            ioe.printStackTrace();
        }

        return sb.toString();
    }

}
